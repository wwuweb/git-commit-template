# README #

Using a standard git commit template will allow the team to have consistent comment styles.

### What is this repository for? ###

This is based on Timothy Merritt's great write up on using [custom git commit templates](https://dev.to/timmybytes/keeping-git-commit-messages-consistent-with-a-custom-template-1jkm)


### How do I get set up? ###

* Clone this repository
* Note the path of the file .commit-conventions.txt
* Run the following command in a terminal
`git config --global commit.template path/to/your/file.txt`
* Then simply type git commit and hit enter when ready to make a commit to use the template as a guide. 
* If you use vim, this will insert your cursor in the right spot to make the commit.
`git config --global core.editor "vim +16 +startinsert"`
